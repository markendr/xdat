package org.nrg.xdat.services.cache;

public interface XnatCache {
    /**
     * Indicates the name of the underlying cache.
     *
     * @return The name of the underlying cache.
     */
    String getCacheName();
}
